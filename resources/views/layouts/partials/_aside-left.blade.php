<!-- BEGIN: Left Aside --><button class="m-aside-left-close  m-aside-left-close--skin-light " id="m_aside_left_close_btn"><i class="la la-close"></i></button>
<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-light ">
	<!-- BEGIN: Aside Menu -->
	<div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-light m-aside-menu--submenu-skin-light " m-menu-vertical="1" m-menu-scrollable="1" m-menu-dropdown-timeout="500">
		<ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
			<li class="m-menu__section m-menu__section--first">
				<h4 class="m-menu__section-text">عمومی</h4>
				<i class="m-menu__section-icon flaticon-more-v2"></i>
			</li>
            <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="?page=inner" class="m-menu__link "><i class="m-menu__link-icon flaticon-suitcase"></i><span class="m-menu__link-text">صفحه اصلی</span></a></li>
			<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
				<a href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon flaticon-layers"></i><span class="m-menu__link-text">اجناس</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
				<div class="m-menu__submenu ">
					<span class="m-menu__arrow"></span>
					<ul class="m-menu__subnav">
						<!-- <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true"><span class="m-menu__link"><span class="m-menu__link-text">Resources</span></span> -->
						</li>
						<li class="m-menu__item " aria-haspopup="true"><a href="?page=inner" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">ثبت اجناس خریداری شده</span></a></li>
						<li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="?page=inner" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">تحویل اجناس</span></a></li>
						<li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="?page=inner" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">توزیع اجناس</span></a></li>
						<li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="?page=inner" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">تسلیمی اجناس داغمه</span></a></li>
					</ul>
				</div>
			</li>
			<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover" m-menu-link-redirect="1">
				<a href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon flaticon-settings"></i><span class="m-menu__link-title">  <span class="m-menu__link-wrap">      <span class="m-menu__link-text">تنضیمات</span>      <span class="m-menu__link-badge"><span class="m-badge m-badge--accent">3</span></span>  </span></span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
				<div class="m-menu__submenu ">
					<span class="m-menu__arrow"></span>
					<ul class="m-menu__subnav">
						<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" m-menu-link-redirect="1"><span class="m-menu__link"><span class="m-menu__link-title">  <span class="m-menu__link-wrap">      <span class="m-menu__link-text">تنظیمات</span> <span class="m-menu__link-badge"><span class="m-badge m-badge--accent">0</span></span>
							</span>
							</span>
							</span>
						</li>
						<li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1" >
							<!-- <a href="rout" class="m-menu__link "><span class="m-menu__link-text">نوعیت مصرفی</span></a> -->
							<router-link to="/">
								<span class="m-menu__link-text">نوعیت مصرفی</span>
							</router-link>
						</li>

						<li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="?page=inner" class="m-menu__link "><span class="m-menu__link-text">کتگور اصلی</span></a></li>
						<li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="?page=inner" class="m-menu__link "><span class="m-menu__link-text">کتگوری فرعی</span></a></li>
						<li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="?page=inner" class="m-menu__link "><span class="m-menu__link-text">جنس</span></a></li>
						<li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="?page=inner" class="m-menu__link "><span class="m-menu__link-text">جزئیات جنس</span></a></li>
						<li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="?page=inner" class="m-menu__link "><span class="m-menu__link-text">واحد پولی</span></a></li>
						<li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="?page=inner" class="m-menu__link "><span class="m-menu__link-text">کمپنی تولدی جنس</span></a></li>
					</ul>
				</div>
			</li>

		</ul>
	</div>
	<!-- END: Aside Menu -->
</div>
<!-- END: Left Aside -->
