<!-- begin:: Page -->
<div class="m-grid m-grid--hor m-grid--root m-page">

    <!--[html-partial:include:{"file":"partials\/_header-base.html"}]/-->
    @include('layouts.partials._header-base')

    <!-- begin::Body -->
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
        @include('layouts.partials._aside-left')
        <!--[html-partial:include:{"file":"partials\/_aside-left.html"}]/-->

        <div class="m-grid__item m-grid__item--fluid m-wrapper">
            {{-- @yield('subheader-default') --}}
        <!--[html-partial:include:{"file":"partials\/_subheader-default.html"}]/-->

            <div class="m-content">
                <router-view></router-view>
            </div>
        </div>
    </div>
    <!-- end:: Body -->


    @include('layouts.partials._footer-default')
    <!--[html-partial:include:{"file":"partials\/_footer-default.html"}]/-->

</div>
<!-- end:: Page -->
@include('layouts.partials._layout-quick-sidebar')

@include('layouts.partials._layout-scroll-top')

@include('layouts.partials._layout-tooltips')

<!--[html-partial:include:{"file":"partials\/_layout-quick-sidebar.html"}]/-->



<!--[html-partial:include:{"file":"partials\/_layout-scroll-top.html"}]/-->



<!--[html-partial:include:{"file":"partials\/_layout-tooltips.html"}]/-->
