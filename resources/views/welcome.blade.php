<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    {{-- <link rel="stylesheet" href="{{asset('')}}"> --}}

    <title>Document</title>
</head>
<body>

    <div id="test">

    </div>

    <script src="{{asset('public/js/app.js')}}"></script>
    <script src="{{asset('public/assets/vendors/base/vendors.bundle.js')}}" type="text/javascript"></script>
    <script src="{{asset('public/assets/demo/demo11/base/scripts.bundle.js')}}" type="text/javascript"></script>
    <script src="{{asset('public/assets/vendors/custom/datatables/datatables.bundle.js')}}" type="text/javascript"></script>
    <script src="{{asset('public/assets/vendors/custom/fullcalendar/fullcalendar.bundle.js')}}" type="text/javascript"></script>
    <script src="{{asset('public/assets/app/js/dashboard.js')}}" type="text/javascript"></script>

    <script src="{{asset('public/assets/app/js/chart/amcharts.js')}}" type="text/javascript"></script>
    <script src="{{asset('public/assets/app/js/chart/serial.js')}}" type="text/javascript"></script>
    <script src="{{asset('public/assets/app/js/chart/radar.js')}}" type="text/javascript"></script>
    <script src="{{asset('public/assets/app/js/chart/pie.js')}}" type="text/javascript"></script>
    <script src="{{asset('public/assets/app/js/chart/polarScatter.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('public/assets/app/js/chart/animate.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('public/assets/app/js/chart/export.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('public/assets/app/js/chart/light.js')}}" type="text/javascript"></script>
    <div class="last_script" id="last_script">
    <script src="{{asset('public/assets/app/js/chart/charts.js')}}" type="text/javascript"></script>
    <!--begin::Page Scripts -->
    <script src="{{asset('public/assets/demo/demo11/custom/crud/datatables/basic/paginations.js')}}" type="text/javascript"></script>
    <!-- Custome JS -->
    <!-- <script src="{{asset('public/assets/js/custome.js')}}" type="text/javascript"></script> -->

   </div>
</body>
</html>
