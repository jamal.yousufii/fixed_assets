/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import BootstrapVue from 'bootstrap-vue'
Vue.use(BootstrapVue)
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import router from './router';
// Set basue URL
import Master from './components/layouts/master.vue';
// Global Errors component
import successMessage from './components/ui_fetures/Success.vue';
//Import Progress Bar
import VueProgressBar from 'vue-progressbar';
// Import VForm
import { Form, HasError, AlertError, AlertSuccess } from 'vform'
// Import Sweat Aleart
import swal from 'sweetalert2'
// Import VeeValidate
import VeeValidate, { Validator } from 'vee-validate';
// Import Vee Validate Attributes and Message
import messagesDr from './lang/dr/ValidationMessages.js';
import attributesDr from './lang/dr/Attributes.js';
import messagesEn from './lang/en/ValidationMessages.js';
import attributesEn from './lang/en/Attributes.js';
// Import and use Vue Select
import vSelect from 'vue-select'
Vue.component('v-select', vSelect)
// Import Jalili Date Picker
import VuePersianDatetimePicker from 'vue-persian-datetime-picker';
import moment from 'moment-jalaali';
moment.updateLocale('fa', {
    jMonths: [
        "حمل",
        "ثور",
        "جوزا",
        "سرطان",
        "اسد",
        "سنبله",
        "میزان",
        "عقرب",
        "قوس",
        "جدی",
        "دلو",
        "حوت"
    ]
});


Vue.use(VuePersianDatetimePicker, {
    name: 'custom-date-picker',
    props: {
        inputFormat: 'YYYY-MM-DD HH:mm',
        format: 'jYYYY-jMM-jDD HH:mm',
        editable: false,
        inputClass: 'form-control my-custom-class-name',
        placeholder: 'Please select a date',
        altFormat: 'YYYY-MM-DD HH:mm',
        color: '#00acc1',
        autoSubmit: false,
        //...
        //... And whatever you want to set as default
        //...
    }
});

// Localization
import i18n from './lang/lang';


window.Swal = swal;

// Toast Global Config
const toast = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3000
});
window.toast = toast;

Vue.component(HasError.name, HasError)
Vue.component(AlertError.name, AlertError)
Vue.component(AlertSuccess.name, AlertSuccess)
window.Form = Form;



const options = {
  thickness: '5px',
  transition: {
    speed: '0.2s',
    opacity: '0.6s',
    termination: 300
  },
  autoRevert: true,
  location: 'top',
  inverse: false
}
Vue.use(VueProgressBar, options)

// Form Validation
Vue.use(VeeValidate, {
  locale: 'dr',
  dictionary: {
    en: { messages: messagesEn, attributes: attributesEn },
    dr: { messages: messagesDr, attributes: attributesDr }
  }
});


/* eslint-disable no-new */
var app = new Vue({
  el: '#test',
  router,
  i18n,
  template: '<Master/>',
  components: {
    Master
  },

})
