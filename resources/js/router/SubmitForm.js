    export default {
      data() {
          return {
              s_errors: {},
              main_data: [],
              item_nature: [],
              loaded: true,
              url: '',
              id: '',
              params: {},
              files: [],
          }
      },
      methods:
      {
        sumbitForm(editMode){
            if(editMode){
                this.updateData();
            }
            else{
                this.insertData();
            }
        },

        //Insert Data Only Inputs
        insertData()
        {
            if(this.loaded)
            {
                this.$Progress.start()
                this.loaded  = false;
                this.success = false;
                this.s_errors  = {};
                axios.post(this.url, this.s_fields)
                .then(response => {
                    this.$Progress.finish();
                    //Success Message
                    toast({
                        type: 'success',
                        title: response.data.message
                    })
                    console.log(response.data);
                    this.s_fields  = {}; //Clear input fields.
                    this.loaded  = true;
                    this.success = true;
                })
                .catch(error => {
                    this.$Progress.fail();
                    this.loaded = true;
                    if(error.response.status===422){
                        this.s_errors = error.response.data.errors || {};
                    }
                })
            }
          },
        // Insert Data with Files
        insertDataWithFile()
        {
            if(this.loaded)
            {
                this.$Progress.start()
                this.loaded  = false;
                this.success = false;
                this.s_errors  = {};

                let formData = new FormData();
                if(this.files)
                {
                  for(var i=0; i < this.files.length; i++)
                  {
                    let file = this.files[i];
                    formData.append('files['+i+']',file);
                  }
                }
                formData.append('fields',JSON.stringify(this.s_fields));

                axios.post(this.url, formData,
                                    {
                                      headers: {
                                          'Content-Type': 'multipart/form-data'
                                      }
                                    }
                ).then(response => {
                    this.$Progress.finish();
                    //Success Message
                    toast({
                        type: 'success',
                        title: response.data.message
                    })
                    console.log(response.data);
                    this.s_fields  = {}; //Clear input fields.
                    this.loaded  = true;
                    this.success = true;
                })
                .catch(error => {
                    this.$Progress.fail();
                    this.loaded = true;
                    if(error.response.status===422){
                        this.s_errors = error.response.data.errors || {};
                    }
                })
            }
          },
          getData()
          {
            this.$Progress.start();
            axios.get(this.url).then(response => {
                this.main_data = response.data;
                console.log(response.data);
                jQuery(document).ready(function(){DatatablesBasicPaginations.init()});
                this.$Progress.finish();
            })
            .catch(error => {
                this.$Progress.fail();
                this.loaded = true;
                if(error.response.status===422){
                    this.s_errors = error.response.data.errors || {};
                }
            })
          },
          getDataForEdit()
          {
            this.$Progress.start();
            axios.get(this.url+'/'+this.$route.params.id)
            .then(response => {
                this.$Progress.finish();
                this.s_fields = response.data || {};
                console.log(this.s_fields);

            })
            .catch(error => {
                this.$Progress.fail();
                if(error.response.status===422){
                    this.s_errors = error.response.data.errors || {};
                }
            })
          },
          updateData()
          {
            if(this.loaded)
             {
                this.$Progress.start()
                this.loaded  = false;
                this.success = false;
                this.s_errors  = {};
                axios.put(this.url+'/'+this.$route.params.id, this.s_fields).then(response => {
                    this.$Progress.finish();
                    //Success Message
                    console.log(response.data);
                    toast({
                        type: 'success',
                        title: response.data.message
                    })
                    this.loaded  = true;
                    this.success = true;

                })
                .catch(error => {
                    this.$Progress.setFailColor('red');
                    this.$Progress.fail();
                    this.loaded = true;
                    if(error.response.status===422){
                        this.s_errors = error.response.data.errors || {};
                    }
                })
             }
          },
          deletData(id)
          {
            Swal({
                title: 'ایا شما مطمین هستید ؟',
                text: "دوباره قابل باز یابی نمیباشد.",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'حذف',
                cancelButtonText: 'لغو',
                }).then((result) => {
                if (result.value) {
                    // Server Request
                    axios
                    .delete(this.url+'/'+id)
                    .then(({ data }) => {
                        // Create Custome Event
                        // Fire.$emit('get-item-nature');
                        Swal(
                            'حذف شد!',
                             data.message,
                            'success'
                        )
                        //Slide UP the removed TR
                        jQuery('tr#'+id).slideUp();
                    })
                    .catch(({ data }) => {
                        this.$Progress.fail();
                    })

                }
                })
          },
          getItem(url)
          {
            this.$Progress.start();
            if(this.loaded)
            {
                axios.get(url).then(response => {
                    this.item_nature = response.data;
                    this.$Progress.finish();
                    console.log(this.item_nature);
                    this.loaded = true;
                })
                .catch(error => {
                    this.$Progress.fail();
                    console.log(error);
                    this.loaded = true;
                })
            }
          },
          validateBeforeSubmit(editMode,hasFile) {
            this.$validator.validateAll().then(result => {
              if (result) {
                // Call Edit Function
                if(editMode)
                {
                  // chekc if form has file call Function with file Upload
                  if(hasFile)
                  {
                    this.updateDataWithFile();
                  }//Call funciton Only with Inputs
                  else
                  {
                    this.updateData(hasFile);
                  }
                }
                // Call Insert Funciton
                else
                {
                  // chekc if form has file call Function with file Upload
                  if(hasFile)
                  {
                    this.insertDataWithFile();
                  }//Call funciton Only with Inputs
                  else
                  {
                    this.insertData(hasFile);
                  }

                }
              }
            });
          },
          getPublicData(url)
          {
            this.$Progress.start();
            if(this.loaded)
            {
                axios.post(url,this.params).then(response => {
                    this.main_data = response.data;
                    this.$Progress.finish();
                    console.log(response.data);
                    this.loaded = true;
                })
                .catch(error => {
                    this.$Progress.fail();
                    console.log(error);
                    this.loaded = true;
                })
            }
          },
          getOptions(url)
          {
            this.$Progress.start();
            if(this.loaded)
            {
                axios.get(url,this.params).then(response => {
                    this.options = response.data;
                    console.log(this.options);
                    this.$Progress.finish();
                    this.loaded = true;
                })
                .catch(error => {
                    this.$Progress.fail();
                    console.log(error);
                    this.loaded = true;
                })
            }
          },
          // Set the file for Upload
          fileUpload(e)
          {
              let uploadedFiles = this.$refs.file.files;
              /*
                Adds the uploaded file to the files array
              */
              for( var i = 0; i < uploadedFiles.length; i++ ){
                this.files.push( uploadedFiles[i] );
              }
          },

          /*
            Removes a select file the user has uploaded
          */
          removeFile( key )
          {
            this.files.splice( key, 1 );
          },
          // Get Request to ther Server Side
          getRequest(url,data_container)
          {
            this.$Progress.start();
            if(this.loaded)
            {
                axios.post(url,this.params).then(response => {
                    this.s_fields[data_container] = response.data;
                    console.log(this.s_fields);
                    this.$Progress.finish();
                    this.loaded = true;
                })
                .catch(error => {
                    this.$Progress.fail();
                    console.log(error);
                    this.loaded = true;
                })
            }
          },
          //Get details
          showItemDetails(url,id,data_container)
          {
            if(id!=undefined)
            {
              this.params.id = id;
              this.getRequest(url,data_container);

            }
          },
       }
    }
