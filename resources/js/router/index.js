import Vue from 'vue'
import Router from 'vue-router'
import $ from 'jquery'; 

import Layout from '../components/layouts/layout.vue';

import DashboardComponenet from '../components/dashboardComponent.vue';
import AssetNatureIndex from '../components/asset_nature/Index.vue';
import AssetNatureCreate from '../components/asset_nature/Create.vue';
import AssetNatureEdit from '../components/asset_nature/Edit.vue';
//Import Main Item Component
import MainItemIndex from '../components/main_item/Index.vue';
import MainItemCreate from '../components/main_item/Create.vue';
import MainItemEdit from '../components/main_item/Edit.vue';

// Import Sub Item Component
import SubItemIndex from '../components/sub_item/Index.vue';
import SubItemCreate from '../components/sub_item/Create.vue';
import SubItemEdit from '../components/sub_item/Edit.vue';
// Import End Item Component
import EndItemIndex from '../components/end_item/Index.vue';
import EndItemCreate from '../components/end_item/Create.vue';
import EndItemEdit from '../components/end_item/Edit.vue';
// Import End Item Details Component
import EndItemDetailsIndex from '../components/end_item_details/Index.vue';
import EndItemDetailsCreate from '../components/end_item_details/Create.vue';
import EndItemDetailsEdit from '../components/end_item_details/Edit.vue';
// Import Vendor Components
import VendorIndex from '../components/vendor/Index.vue';
import VendorCreate from '../components/vendor/Create.vue';
import VendorEdit from '../components/vendor/Edit.vue';

// Import Currency Components
import CurrencyIndex from '../components/currency/Index.vue';
import CurrencyCreate from '../components/currency/Create.vue';
import CurrencyEdit from '../components/currency/Edit.vue';

// Import Currency Components
import UnitIndex from '../components/unit/Index.vue';
import UnitCreate from '../components/unit/Create.vue';
import UnitEdit from '../components/unit/Edit.vue';

// Import Iteqm Inquire Components
import ItemInquireIndex from '../components/item_inquire/Index.vue';
import ItemInquireCreate from '../components/item_inquire/Create.vue';
import ItemInquireEdit from '../components/item_inquire/Edit.vue';
import ItemInquireView from '../components/item_inquire/View.vue';
// Import Inventory Component
import InquireListIndex from '../components/inventory/inquire_list.vue';

// Import Item Purchase Components
import PurchaseIndex from '../components/purchase/Index.vue';
import PurchaseCreate from '../components/purchase/Create.vue';
import PurchaseEdit from '../components/purchase/Edit.vue';
import PurchaseView from '../components/purchase/View.vue';

// Import Item Inward Components
import InwardIndex from '../components/inward/Index.vue';
import InwardCreate from '../components/inward/Create.vue';
import InwardEdit from '../components/inward/Edit.vue';
import InwardView from '../components/inward/View.vue';

// Import Allotment Component
import AllotmentIndex from '../components/allotment/Index.vue';
import AllotmentCreate from '../components/allotment/Create.vue';
import AllotmentEdit from '../components/allotment/Edit.vue';
import AllotmentView from '../components/allotment/View.vue';

// Import Allotment To Individual
import AllotToIndividualIndex from '../components/allot_to_individual/Index.vue';
import AllotToIndividualCreate from '../components/allot_to_individual/Create.vue';
import AllotToIndividualEdit from '../components/allot_to_individual/Edit.vue';
import AllotToIndividualView from '../components/allot_to_individual/View.vue';

// Import Writeoff components
import WriteoffIndex from '../components/writeoff/Index.vue';
import WriteoffCreate from '../components/writeoff/Create.vue';
import WriteoffEdit from '../components/writeoff/Edit.vue';
import WriteoffView from '../components/writeoff/View.vue';


Vue.use(Router)

export default new Router({

	routes: [{
			path: '/',
			name: 'Home',
			redirect: '/dashboard',
			component: Layout,
			meta: {
				title: 'Dashboard',
			},
			children: [{
					path: '/dashboard',
					name: 'DashboardComponenet',
					component: DashboardComponenet,
					meta: {
						title: 'صفحه اصلی',
						description: 'lorem'
					},
				},
				{
					path: '/item_nature',
					name: 'ItemNatureIndex',
					component: AssetNatureIndex,
					meta: {
						title: 'نوعیت مصرفی جنس',
						create_url: '/item_nature/create',
						description: 'lorem',
						parent: 'setting'
					}

				},
				{
					path: '/item_nature/create',
					name: 'ItemNatureCreate',
					component: AssetNatureCreate,
					meta: {
						title: 'اضافه نمودن نوعیت مصرفی جنس',
						description: 'lorem',
						parent: 'setting',
						editMode: false,
						create_btn_label: 'ذخیره',
					},

				},
				{
					path: '/item_nature/edit/:id',
					name: 'EditAssetNature',
					component: AssetNatureEdit,
					meta: {
						title: 'تجدید نمودن نوعیت مصرفی جنس',
						description: 'lorem',
						parent: 'setting',
						editMode: true,
						create_btn_label: 'تجدید',
					},

				},
				{
					path: '/main_category',
					name: 'MainItemIndex',
					component: MainItemIndex,
					meta: {
						title: 'جنس عمومی',
						create_url: '/main_category/create',
						description: 'lorem',
						parent: 'setting'
					},

				},
				{
					path: '/main_category/create',
					name: 'MainItemCreate',
					component: MainItemCreate,
					meta: {
						title: 'اضافه نمودن جنس عمومی',
						description: 'lorem',
						parent: 'setting',
						btn_label: 'ذخیره',
						editMode: false,
					},

				},
				{
					path: '/main_item/edit/:id',
					name: 'MainItemEdit',
					component: MainItemEdit,
					meta: {
						title: 'تجدید نمودن جنس عمومی',
						description: 'lorem',
						parent: 'setting',
						btn_label: 'تجدید',
						editMode: true
					},

				},
				{
					path: '/sub_item',
					name: 'SubItemIndex',
					component: SubItemIndex,
					meta: {
						title: 'جنس فرعی',
						create_url: '/sub_item/create',
						description: 'lorem',
						parent: 'setting'
					},

				},
				{
					path: '/sub_item/create',
					name: 'SubItemCreate',
					component: SubItemCreate,
					meta: {
						title: 'اضافه نمودن جنس فرعی',
						description: 'lorem',
						parent: 'setting',
						btn_label: 'ذخیره',
						editMode: false,
					},

				},
				{
					path: '/sub_item/edit/:id',
					name: 'SubItemEdit',
					component: SubItemEdit,
					meta: {
						title: 'تجدید نمودن جنس فرعی',
						description: 'lorem',
						parent: 'setting',
						btn_label: 'تجدید',
						editMode: true,
					},
				},
				{
					path: '/end_item',
					name: 'EndItemIndex',
					component: EndItemIndex,
					meta: {
						title: 'جنس',
						create_url: '/end_item/create',
						description: 'lorem',
						parent: 'setting'
					},

				},
				{
					path: '/end_item/create',
					name: 'EndItemCreate',
					component: EndItemCreate,
					meta: {
						title: 'اضافه نمودن جنس ',
						description: 'lorem',
						btn_label: 'ذخیره',
						parent: 'setting',
						editMode: false,
					},

				}
				,
				{
					path: '/end_item/edit/:id',
					name: 'EndItemEdit',
					component: EndItemEdit,
					meta: {
						title: 'تجدید نمودن جنس',
						description: 'lorem',
						btn_label: 'تجدید',
						parent: 'setting',
						editMode: true,
					},
				},
				{
					path: '/end_item_details',
					name: 'EndItemDetailsIndex',
					component: EndItemDetailsIndex,
					meta: {
						title: 'جزیئات جنس',
						create_url: '/end_item_details/create',
						description: 'lorem',
						parent: 'setting'
					},
			  },
				{
					path: '/end_item_details/create',
					name: 'EndItemDetailsCreate',
					component: EndItemDetailsCreate,
					meta: {
						title: 'اضافه نمودن جزیئات جنس',
						description: 'lorem',
						parent: 'setting',
						btn_label: 'ذخیره',
						editMode: false,
					},

				}
				,
				{
					path: '/end_item_details/edit/:id',
					name: 'EndItemDetailsEdit',
					component: EndItemDetailsEdit,
					meta: {
						title: 'تجدید نمودن جزیئات جنس',
						description: 'lorem',
						parent: 'setting',
						btn_label: 'تجدید',
						editMode: true,
					},
				},
				{
					path: '/vendor',
					name: 'VendorIndex',
					component: VendorIndex,
					meta: {
						title: 'کمپنی تولیدی جنس',
						create_url: '/vendor/create',
						description: 'lorem',
						parent: 'setting'
					},
			  },
				{
					path: '/vendor/create',
					name: 'VendorCreate',
					component: VendorCreate,
					meta: {
						title: 'اضافه نمودن کمپنی تولیدی جنس',
						description: 'lorem',
						parent: 'setting',
						btn_label: 'ذخیره',
						editMode: false,
					},

				},
				{
					path: '/vendor/edit/:id',
					name: 'VendorEdit',
					component: VendorEdit,
					meta: {
						title: 'تجدید نمودن کمپنی تولیدی',
						description: 'lorem',
						parent: 'setting',
						btn_label: 'تجدید',
						editMode: true,
					},
				}
				,
				{
					path: '/currency',
					name: 'CurrencyIndex',
					component: CurrencyIndex,
					meta: {
						title: 'واحد پولی',
						create_url: '/currency/create',
						description: 'lorem',
						parent: 'setting'
					},
			  },
				{
					path: '/currency/create',
					name: 'CurrencyCreate',
					component: CurrencyCreate,
					meta: {
						title: 'اضافه نمودن واحد پولی',
						description: 'lorem',
						parent: 'setting',
						btn_label: 'ذخیره',
						editMode: false,
					},
				},
				{
					path: '/currency/edit/:id',
					name: 'CurrencyEdit',
					component: CurrencyEdit,
					meta: {
						title: 'تجدید نمودن واحد پولی',
						description: 'lorem',
						parent: 'setting',
						btn_label: 'تجدید',
						editMode: true,
					},
				},
				//unit route
				{
					path: '/unit',
					name: 'UnitIndex',
					component: UnitIndex,
					meta: {
						title: 'واحد',
						create_url: '/unit/create',
						description: 'lorem',
						parent: 'setting'
					},
			  },
				{
					path: '/unit/create',
					name: 'UnitCreate',
					component: UnitCreate,
					meta: {
						title: 'اضافه نمودن واحد',
						description: 'lorem',
						parent: 'setting',
						btn_label: 'ذخیره',
						editMode: false,
					},
				},
				{
					path: '/unit/edit/:id',
					name: 'UnitEdit',
					component: UnitEdit,
					meta: {
						title: 'تجدیدنمودن واحد',
						description: 'lorem',
						parent: 'setting',
						btn_label: 'تجدید',
						editMode: true,
					},
				},
				// Items Children Route
				{
					path: '/items/inquire',
					name: 'ItemInquireIndex',
					component: ItemInquireIndex,
					meta: {
						title: 'درخواستی جنس',
						create_url: '/items/inquire/create',
						description: 'lorem',
						parent: 'items'
					},
				},
				{
					path: '/items/inquire/create',
					name: 'ItemInquireCreate',
					component: ItemInquireCreate,
					meta: {
						title: 'ثبت درخواستی جنس',
						description: 'lorem',
						parent: 'items',
						btn_label: 'ذخیره',
						editMode: false,
					},
				},
				{
					path: '/items/inquire/view/:id',
					name: 'ItemInquireView',
					component: ItemInquireView,
					meta: {
						title: 'نمایش درخواستی جنس',
						description: 'lorem',
						parent: 'items',
						btn_label: 'ذخیره',
						editMode: true,
					},
				},
				{
					path: '/items/inquire/edit/:id',
					name: 'ItemInquireEdit',
					component: ItemInquireEdit,
					meta: {
						title: 'تجدید درخواستی جنس',
						description: 'lorem',
						parent: 'items',
						btn_label: 'تجدید',
					},
				},
				// purchase routes
				{
					path: '/items/purchase',
					name: 'PurchaseIndex',
					component: PurchaseIndex,
					meta: {
						title: 'خریداری اجناس',
						create_url: '/items/purchase/create',
						description: 'lorem',
						parent: 'items'
					},
				},
				{
					path: '/items/purchase/create',
					name: 'PurchaseCreate',
					component: PurchaseCreate,
					meta: {
						title: 'ثبت جنس خریداری شده',
						description: 'lorem',
						parent: 'items',
						btn_label: 'ذخیره',
						editMode: false,
					},
				},
				{
					path: '/items/purchase/edit/:id',
					name: 'PurchaseEdit',
					component: PurchaseEdit,
					meta: {
						title: 'تصحیح جنس خریداری شده',
						description: 'lorem',
						parent: 'items',
						btn_label: 'تجدید',
						editMode: true,

					},
				},
				{
					path: '/items/purchase/view/:id',
					name: 'PurchaseView',
					component: PurchaseView,
					meta: {
						title: 'جزیيات خریداری جنس',
						description: 'lorem',
						parent: 'items',
						btn_label: 'ذخیره',
						editMode: true,
					},
				},
				// item inward routes
				{
					path: '/items/inward',
					name: 'InwardIndex',
					component: InwardIndex,
					meta: {
						title: 'رسید جنس',
						create_url: '/items/inward/create',
						description: 'lorem',
						parent: 'items'
					},
				},
				{
					path: '/items/inward/create',
					name: 'InwardCreate',
					component: InwardCreate,
					meta: {
						title: 'رسید کردن جنس',
						description: 'lorem',
						parent: 'items',
						btn_label: 'ذخیره',
						editMode: false,
					},
				},
				{
					path: '/items/inward/edit/:id',
					name: 'InwardEdit',
					component: InwardEdit,
					meta: {
						title: 'تصحیح رسید جنس',
						description: 'lorem',
						parent: 'items',
						btn_label: 'تجدید',
						editMode: true,

					},
				},
				{
					path: '/items/inward/view/:id',
					name: 'InwardView',
					component: InwardView,
					meta: {
						title: 'نمایشد جنس رسید شده',
						description: 'lorem',
						parent: 'items',
						btn_label: 'ذخیره',
						editMode: true,
					},
				},
				{
					path: '/items/inquire_list',
					name: 'InquireListIndex',
					component: InquireListIndex,
					meta: {
						title: 'لیست درخواستی جنس',
						create_url: '/items/inquire/create',
						description: 'lorem',
						parent: 'items'
					},
				},
				// Allotment Route
				{
					path: '/items/allotment',
					name: 'AllotmentIndex',
					component: AllotmentIndex,
					meta: {
						title: 'توزیع اجناس',
						create_url: '/items/allotment/create',
						description: 'lorem',
						parent: 'items'
					},
				},
				{
					path: '/items/allotment/create',
					name: 'AllotmentCreate',
					component: AllotmentCreate,
					meta: {
						title: 'توزیع نمودن اجناس',
						description: 'lorem',
						parent: 'items',
						btn_label: 'ذخیره',
						editMode: false,
					},
				},
				{
					path: '/items/allotment/edit/:id',
					name: 'AllotmentEdit',
					component: AllotmentEdit,
					meta: {
						title: 'تصحیح نمودن اجناس توزیع شده',
						description: 'lorem',
						parent: 'items',
						btn_label: 'تجدید',
						editMode: true,

					},
				},
				{
					path: '/items/allotment/view/:id',
					name: 'AllotmentView',
					component: AllotmentView,
					meta: {
						title: 'نمایش اجناس توزیع شده',
						description: 'lorem',
						parent: 'items',
						btn_label: 'ذخیره',
						editMode: true,
					},
				},
				// Allot To Individual
				{
					path: '/items/allot_to_individual',
					name: 'AllotToIndividualIndex',
					component: AllotToIndividualIndex,
					meta: {
						title: 'توزیع جنس به شخص',
						create_url: '/items/allot_to_individual/create',
						description: 'lorem',
						parent: 'items'
					},
				},
				{
					path: '/items/allot_to_individual/create',
					name: 'AllotToIndividualCreate',
					component: AllotToIndividualCreate,
					meta: {
						title: 'توزیع نمودن اجناس',
						description: 'lorem',
						parent: 'items',
						btn_label: 'ذخیره',
						editMode: false,
					},
				},
				{
					path: '/items/allot_to_individual/edit/:id',
					name: 'AllotToIndividualEdit',
					component: AllotToIndividualEdit,
					meta: {
						title: 'تصحیح نمودن اجناس توزیع شده',
						description: 'lorem',
						parent: 'items',
						btn_label: 'تجدید',
						editMode: true,

					},
				},
				{
					path: '/items/allot_to_individual/view/:id',
					name: 'AllotToIndividualView',
					component: AllotToIndividualView,
					meta: {
						title: 'نمایش اجناس توزیع شده',
						description: 'lorem',
						parent: 'items',
						btn_label: 'ذخیره',
						editMode: true,
					},
				},
				// Writeoff route
				{
					path: '/items/writeoff',
					name: 'WriteoffIndex',
					component: WriteoffIndex,
					meta: {
						title: 'تسلیمی اجناس داغمه',
						create_url: '/items/writeoff/create',
						description: 'lorem',
						parent: 'items'
					},
				},
				{
					path: '/items/writeoff/create',
					name: 'WriteoffCreate',
					component: WriteoffCreate,
					meta: {
						title: 'ثبت تسلیمی اجناس داغمه',
						description: 'lorem',
						parent: 'items',
						btn_label: 'ذخیره',
						editMode: false,
					},
				},
				{
					path: '/items/writeoff/edit/:id',
					name: 'WriteoffEdit',
					component: WriteoffEdit,
					meta: {
						title: 'تصحیح اجناس داغمه ثبت شده',
						description: 'lorem',
						parent: 'items',
						btn_label: 'تجدید',
						editMode: true,

					},
				},
				{
					path: '/items/writeoff/view/:id',
					name: 'WriteoffView',
					component: WriteoffView,
					meta: {
						title: 'نمایش اجناس داغمه',
						description: 'lorem',
						parent: 'items',
						btn_label: 'ذخیره',
						editMode: true,
					},
				},
			]
		},
	]
})
