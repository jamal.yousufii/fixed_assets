export default {
    item_nature_dr: 'نوعیت مصرفی به دری',
    item_nature_pa: 'نوعیت مصرفی به پشتو',
    item_nature_en: 'نوعیت مصرفی به انگلسی',
    main_item_name_dr: 'نام جنس عمومی به دری',
    main_item_name_pa: 'نام جنس عمومی به پشتو',
    main_item_name_en: 'نام جنس عمومی به انگلسی',
    main_item_id: 'جنس عمومی',
    sub_item_name_dr: 'جنس فرعی به دری',
    sub_item_name_pa: 'جنس فرعی به پشتو',
    sub_item_name_en: 'جنس فرعی به انگلسی',
    end_item_name_dr: 'نام جنس به دری',
    end_item_name_pa: 'نام جنس به پشتو',
    end_item_name_en: 'نام جنس به انگلسی',
    // Vendor Validation Attributes
    vendor_name_dr: 'نام کمپنی به دری',
    vendor_name_pa: 'نام کمپنی به پشتو',
    vendor_name_en: 'نام کمپنی به انگلسی',
    // Currency Validation Attributes
    currency_name_dr: 'واحید پولی به دری',
    currency_name_pa: 'واحید پولی به پشتو',
    currency_name_en: 'واحید پولی به انگلسی',
    // Unit Validation Attributes
    unit_name_dr: ' واحد دری',
    unit_name_pa: 'واحد پشتو',
    unit_name_en: 'واحد انگلسی',
    // Item Detials Attributes
    end_item_details_dr: 'جزیئات جنس به دری',
    end_item_details_pa: 'جزیئات جنس به پشتو',
    end_item_details_en: 'جزیئات جنس به انگلسی',
    // Item Detials Attributes
    item_inquire_number: 'نمبر درخواستی',
    item_inquire_date: 'تاریخ درخواستی',
    item_inquire_desc: 'توضیحات درخواستی',
}
