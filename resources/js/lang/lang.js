import Vue from 'vue'
import VueI18n from 'vue-i18n'

import dr from './dr/Localization.json';

Vue.use(VueI18n)

const locale = 'dr'

const messages = {
    dr
}

const i18n = new VueI18n({
    /** 默认值 */
    locale,
    messages
})

export default i18n