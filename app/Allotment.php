<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Allotment extends Model
{
     use SoftDeletes;
    /*
    * Get the inward that own the allotment
    *
    */
    public function inward()
    {
       return $this->belongsTo('App\Inward');
    }

    /*
    * Get the writeoff for the allotment
    *
    */
    public function writeOff()
    {
        return $this->hasMany('App\WriteOff');
    }
}
