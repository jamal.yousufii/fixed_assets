<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Inward extends Model
{
     use SoftDeletes;

     /**
      * The attributes that are mass assignable.
      *
      * @var array
      */
     protected $fillable = ['purchase_id','item_details_id','m7_number','warranty','inward_date','dept_id','store_keeper_id', 'authorized_by', 'number_of_item', 'remain', 'created_by', 'status'];

    // Get the purchase that own the Inward
    public function purchase()
    {
       return $this->belongsTo('App\Purchase');
    }

    // Get the allotment for inward
    public function allotment()
    {
       return $this->hasMany('App\Allotment');
    }

    // Get End Item Details
    public function endItemDetails()
    {
       return $this->belongsTo('App\EndItemDetails','item_details_id','id'); 
    }
}
