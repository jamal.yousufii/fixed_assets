<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemInquire extends Model
{
     use SoftDeletes;
    protected $fillable = ['item_inquire_number','item_inquire_desc','item_inquire_date','created_by','status'];

    public function inquireStatus()
    {
      return $this->belongsTo('App\StaticTable','status','code');
    }
}
