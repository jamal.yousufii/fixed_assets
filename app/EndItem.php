<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EndItem extends Model
{
     use SoftDeletes;
    protected $fillable = ['sub_item_id', 'end_item_code', 'end_item_name_en', 'end_item_name_dr', 'end_item_name_pa', 'status', 'created_by'];

    /*
    * Get the Sub Item that own the End Item
    *
    */
    public function subItem()
    {
        return $this->belongsTo('App\SubItem','sub_item_id');
    }

    /**
    * Get the End Item Details for the End Item
    *
    */
    public function endItemDetails()
    {
        return $this->hasMany('App\EndItemDetails');
    }
}
