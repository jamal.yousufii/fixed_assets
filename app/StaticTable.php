<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StaticTable extends Model
{
     use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['code','name_en','name_dr','name_pa','type','created_by'];

    // Get the End Items for Vendor
    public function endItemDetails()
    {
        return $this->hasMany('App\EndItemDetails','vendor_id','id');
    }

    //
}
