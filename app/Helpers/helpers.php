<?php
if(!function_exists('get_field')){
  function get_field($table,$field,$cond)
  {
    return \DB::table($table)->select($field)->where($cond)->first();
  }

  // Generate code for Sub Item Code
  if(!function_exists('generate_code'))
  {
      function generate_code($mainCode,$subCode)
      {
        $subCode = array_last(explode('-',$subCode->sub_item_code));
        return $mainCode->input('main_item.main_item_code').'-'.$subCode;
      }
  }

  // Generate code for End Item Code
  if(!function_exists('generateCodeForEndItem'))
  {
      /**
       * [generateCodeForEndItem description]
       * @param  [type] $mainCode [description]
       * @param  [type] $subCode  [description]
       * @return [type]           [description]
       */
      function generateCodeForEndItem($mainCode,$subCode)
      {
        $subCode = array_last(explode('-',$subCode->end_item_code));
        return $mainCode->input('sub_item.sub_item_code').'-'.$subCode;
      }
  }

  if (!function_exists('endItemDetailsCode'))
  {
     function endItemDetailsCode($mainCode,$subCode)
     {
       $subCode = array_last(explode('-',$subCode->item_details_code));
       return $mainCode->input('end_item.end_item_code').'-'.$subCode;
     }
  }

  /**
   * Upload Multiple File
   * @param  array  All Input Request
   * @param  integer Record ID from Database
   * @param  string Location of the file to store
   * @return booleans
   */
  function uploadFile($request,$row_id,$location)
  {
    if(count($request->file('files'))>0)
    {
      $i    = 1;
      $path = '';
      foreach($request->file('files') as $file)
      {
         $extension = $file->extension();
         $path = $file->storeAs($location,$row_id."_$i.".$extension);
        $i++;
      }

      return $path;
    }
  }
}


?>
