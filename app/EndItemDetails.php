<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EndItemDetails extends Model
{
     use SoftDeletes;

    protected $fillable = ['vendor_id','end_item_id','item_details_code','end_item_details_dr','end_item_details_pa','end_item_details_en','status', 'created_by'];

    /*
    * Get the End Item that owns the End Item Detials
    *
    */
    public function endItem()
    {
       return $this->belongsTo('App\EndItem');
    }

    /*
    * Get the Purchase items for the End Item
    *
    */
    public function purchase()
    {
        return $this->hasMany('App\Purchase');
    }

    /*
    * Get the vendor that owns the End Items
    *
    */
    public function vendor()
    {
        return $this->belongsTo('App\StaticTable','vendor_id','id')->where('type','vendor');
    }

}
