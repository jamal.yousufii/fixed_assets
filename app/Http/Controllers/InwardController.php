<?php

namespace App\Http\Controllers;

use App\Inward;
use Illuminate\Http\Request;
use App\Purchase;
use App\Http\Resources\PurchaseResource;
use App\StaticTable;
class InwardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Inward::with('endItemDetails')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $custom_fields = array(
                    'item_details_id' => $request->input('purchased_item.id'),
                    'purchase_id'     => $request->input('purchased_item.vendor_id'),
                    'dept_id'         => $request->input('department.id'),
                    'store_keeper_id' => $request->input('store_keeper.id'),
                    'warranty'        => $request->input('warranty.id'),
                    'number_of_item'  => $request->number_of_item,
                    'inward_date'     => $request->inward_date,
                    'm7_number'       => $request->m7_number,
                    'remain'          => $request->number_of_item,
                    'created_by'      => 1,
                    'status'          => 1,
        );

        Inward::create($custom_fields);

        return response()->json([
            'status' => 'success',
            'message' => 'جنس خریداری شده موفقانه رسید گردید.',
        ]);


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Inward  $inward
     * @return \Illuminate\Http\Response
     */
    public function show(Inward $inward)
    {
       return $inward::with('endItemDetails')->first();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Inward  $inward
     * @return \Illuminate\Http\Response
     */
    public function edit(Inward $inward)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Inward  $inward
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Inward $inward)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Inward  $inward
     * @return \Illuminate\Http\Response
     */
    public function destroy(Inward $inward)
    {
        //
    }

   /**
   * Get Inward Options
   * @return array
   */
    public function getInwardOptions()
    {
      $purchase =  Purchase::with('endItemDetails')->where('status','0')->get();
      $purchase = $purchase->toArray();
      $end_item_details = [];
      $i = 0;
      foreach($purchase as $pr)
      {
         $end_item_details[$i] = $pr['end_item_details'];
         $end_item_details[$i]['purchase_id'] = $pr['id'];
         $i++;
      }

      return [
        'purchase' => $end_item_details,
        'warranty' => StaticTable::where('type','warranty')->get(),
        'departments' => StaticTable::where('type','department')->get(),
        'store_keeper' => StaticTable::where('type','store_keeper')->get(),
      ];
    }

    public function inwardDetails(Request $request)
    {
         return Inward::with('endItemDetails')->find($request->id);
    }
}
