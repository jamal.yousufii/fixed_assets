<?php

namespace App\Http\Controllers;

use App\MainItem;
use Illuminate\Http\Request;
use App\Http\Requests\MainItemRequest;
use App\Http\Resources\MainItemResource;

class MainItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return MainItemResource::collection(MainItem::with('itemNature')->get());

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MainItemRequest $request)
    {
        $item_nature_code = $request->input('item_nature.item_nature_code');
        $main_item_code  = sprintf("%'.02d\n", MainItem::count()+1);
        $custome_fields = array(
                            'main_item_code' => $item_nature_code.'-'.$main_item_code,
                            'item_nature_id' => $request->input('item_nature.id'),
                            'status'         => 1,
                            'created_by'     => 1,
                         );

        $result = MainItem::create($request->all() + $custome_fields);

        return response()->json([
                'message' => 'کتگوری اصلی جنس اضافه گردید.',
                'status'  => 'success'
            ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MainItem  $mainItem
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new MainItemResource(MainItem::with('itemNature')->find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MainItem  $mainItem
     * @return \Illuminate\Http\Response
     */
    public function edit(MainItem $mainItem)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MainItem  $mainItem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MainItem $mainItem)
    {
        /**
         * $item_nature_code, $main_item_code For generating the code for Main Item  e.g. 01, 02 => 01-02
         * @var [type]
         */
        $item_nature_code = $request->input('item_nature.item_nature_code');
        $main_item_code   = array_last(explode('-', $mainItem->main_item_code));
        $request_inputs   = $request->all();

        $request_inputs['main_item_code'] = "$item_nature_code-$main_item_code";
        $request_inputs['item_nature_id'] = $request->input('item_nature.id');
        $mainItem->update($request_inputs);

        return response()->json([
            'success' => 'success',
            'message' => 'نوعیت مصرفی جنس موفقانه تجدید گردید.'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MainItem  $mainItem
     * @return \Illuminate\Http\Response
     */
    public function destroy(MainItem $mainItem)
    {
        $mainItem->status = 0;
        $mainItem->save();

        return response()->json([
            'success' => 'success',
            'message' => 'ریکارد موفقانه حذف گردید.'
        ]);
    }
}
