<?php

namespace App\Http\Controllers;

use App\Inward;
use App\Allotment;
use Illuminate\Http\Request;
use App\StaticTable;

class AllotmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         return Allotment::with(['inward','inward.endItemDetails'])->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $allotment = new Allotment;
        $allotment->inward_id = $request->input('inward_item.inward_id');
        $allotment->assigned_by = $request->input('assigned_by.id');
        $allotment->assigned_to = $request->input('assigned_to.id');
        $allotment->fees5_number = $request->fees5_number;
        $allotment->allot_date	 = $request->allot_date;
        $allotment->sanction_by = $request->input('sanction_by.id');
        $allotment->number_of_item = $request->number_of_item;
        $allotment->type = 1;
        $allotment->status = 1;
        $allotment->created_by = 1;
        $allotment->save();

        return response()->json([
            'message' => 'جنس خریداری موفقانه ثبت گردید',
            'status'  => 'success',
        ]);


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Allotment  $allotment
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       return Allotment::with(['inward','inward.endItemDetails'])->find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Allotment  $allotment
     * @return \Illuminate\Http\Response
     */
    public function edit(Allotment $allotment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Allotment  $allotment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Allotment $allotment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Allotment  $allotment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Allotment $allotment)
    {
        //
    }

    /**
     * Get Allotment Options
     * @return array
     */
    public function getAllotmentOptions()
    {
        $inward =  Inward::with('endItemDetails')->where('status','1')->get();
        $inward = $inward->toArray();
        $end_item_details = [];
        $i = 0;
        foreach($inward as $pr)
        {
           $end_item_details[$i] = $pr['end_item_details'];
           $end_item_details[$i]['inward_id'] = $pr['id'];
           $i++;
        }

       return [
          'directorate' => StaticTable::where('type','directorate')->get(),
          'department'  => StaticTable::where('type','department')->get(),
          'assigned_to' => StaticTable::where('type','assigned_to')->get(),
          'assigned_by' => StaticTable::where('type','store_keeper')->get(),
          'sanction_by' => StaticTable::where('type','sanction_by')->get(),
          'inward_item' => $end_item_details,
       ];
    }
}
