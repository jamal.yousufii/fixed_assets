<?php

namespace App\Http\Controllers;

use App\EndItemDetails;
use App\EndItem;
use App\StaticTable;
use App\Http\Requests\EndItemDetailsRequest;
use App\Http\Resources\EndItemDetailsResource;
use App\Http\Resources\EndItemResource;
use Illuminate\Http\Request;
class EndItemDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return EndItemDetailsResource::collection(EndItemDetails::with('endItem')->with('vendor')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EndItemDetailsRequest $request)
    {
        $end_item_details_code = sprintf("%'.02d\n",EndItemDetails::count()+1);
        $end_item_code         = $request->input('end_item.end_item_code');
        $custom_fields = [
            'vendor_id'         => $request->input('vendor.id'),
            'end_item_id'       => $request->input('end_item.id'),
            'item_details_code' => "$end_item_code-$end_item_details_code",
            "status" => 1,
            "created_by" => 1
        ];

        EndItemDetails::create($request->all()+$custom_fields);

        return response()->json([
                'message' => 'جزیئات جنس موفقانه ثبت گردید.',
                'status'  => 'success'
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EndItemDetails  $endItemDetails
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new EndItemDetailsResource(EndItemDetails::with('endItem')->with('vendor')->find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EndItemDetails  $endItemDetails
     * @return \Illuminate\Http\Response
     */
    public function edit(EndItemDetails $endItemDetails)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EndItemDetails  $endItemDetails
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $endItemDetails = EndItemDetails::find($id);
       $endItemDetails->end_item_id = $request->input('end_item.id');
       $endItemDetails->item_details_code = endItemDetailsCode($request,$endItemDetails);
       $endItemDetails->end_item_details_dr   = $request->end_item_details_dr;
       $endItemDetails->end_item_details_pa   = $request->end_item_details_pa;
       $endItemDetails->end_item_details_en   = $request->end_item_details_en;
       $endItemDetails->save();


       return response()->json([
               'message' => 'جزیئات جنس موفقانه تجدید گردید.',
               'status'  => 'success'
           ]);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EndItemDetails  $endItemDetails
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $endItemDetails = EndItemDetails::find($id);
        $endItemDetails->status = 0;
        $endItemDetails->save();

        return response()->json([
                'message' => 'جزیئات جنس موفقانه حذف گردید.',
                'status'  => 'success'
            ]);
    }
    /**
     * Get End Item and Vendor for End Item Details
     * @param string $value [description]
     */
    public function endDetailsItemOption()
    {

       return [
          'end_item' =>  EndItem::all(),
          'vendor'   => StaticTable::where('type','vendor')->get()
       ];
    }
}
