<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class PublicController extends Controller
{
    //Get Public Data
    public function getPublicData(Request $request)
    {
        $condition = $request->condition;
        // return $condition;
        $result = DB::table($request->table_name)
                          ->when($condition,function($result,$condition){
                              return $result->where($condition);
                          })->whereNull('deleted_at')
                          ->get();
        return $result;
    }
}
