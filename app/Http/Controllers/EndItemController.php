<?php

namespace App\Http\Controllers;

use App\EndItem;
use Illuminate\Http\Request;
use App\Http\Requests\ItemRequest;
use App\Http\Resources\EndItemResource;
class EndItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return EndItemResource::collection(EndItem::with('subItem')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return "hey";
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ItemRequest $request)
    {
      $total_record   = sprintf("%'.02d\n", EndItem::count()+1);
      $sub_item_code  = $request->input('sub_item.sub_item_code');
      $sub_item_id    = $request->input('sub_item.id');
      $custom_fields  = array(
                          'sub_item_id'   => $sub_item_id,
                          'end_item_code'  => "$sub_item_code-$total_record",
                          'status'         => 1,
                          'created_by'     => 1,
                       );

      $result = EndItem::create($request->all() + $custom_fields);

      return response()->json([
              'message' => 'جنس موفقانه ثبت گردید.',
              'status'  => 'success'
          ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EndItem  $endItem
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new EndItemResource(EndItem::with('subItem')->find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EndItem  $endItem
     * @return \Illuminate\Http\Response
     */
    public function edit(EndItem $endItem)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EndItem  $endItem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EndItem $endItem)
    {
        $request_input = $request->all();
        $request_input['sub_item_id']   = $request->input('sub_item.id');
        $request_input['end_item_code'] = generateCodeForEndItem($request,$endItem);
        $endItem->update($request_input);

        return response()->json([
            'success' => 'success',
            'message' => 'جنس موفقانه تجدید گردید.'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EndItem  $endItem
     * @return \Illuminate\Http\Response
     */
    public function destroy(EndItem $endItem)
    {
      $endItem->status = 0;
      $endItem->save();

      return response()->json([
          'success' => 'success',
          'message' => 'ریکارد موفقانه حذف گردید.'
      ]);
    }
}
