<?php

namespace App\Http\Controllers;

use App\SubItem;
use Illuminate\Http\Request;
use App\Http\Requests\SubItemRequest;
use App\Http\Resources\SubItemResource;

class SubItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return SubItemResource::collection(SubItem::with('mainItem')->get());

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SubItemRequest $request)
    {
        $total_record   = sprintf("%'.02d\n", SubItem::count()+1);
        $main_item_code = $request->input('main_item.main_item_code');
        $main_item_id   = $request->input('main_item.id');
        $all_input      = $request->except('main_item');
        $custom_fields = array(
                            'main_item_id'   => $main_item_id,
                            'sub_item_code'  => $main_item_code.'-'.$total_record,
                            'status'         => 1,
                            'created_by'     => 1,
                         );

        $result = SubItem::create($all_input + $custom_fields);

        return response()->json([
                'message' => 'کتگوری فرعی جنس  موفقانه اضافه گردید.',
                'status'  => 'success'
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SubItem  $subItem
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        return new SubItemResource(SubItem::with('mainItem')->find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SubItem  $subItem
     * @return \Illuminate\Http\Response
     */
    public function edit(SubItem $subItem)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SubItem  $subItem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SubItem $subItem)
    {
      $request_input = $request->all();
      $request_input['main_item_id'] = $request->input('main_item.id');
      $request_input['sub_item_code'] = generate_code($request,$subItem);

      $subItem->update($request_input);

      return response()->json([
          'success' => 'success',
          'message' => 'جنس فرعی موفقانه تجدید گرید.'
      ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SubItem  $subItem
     * @return \Illuminate\Http\Response
     */
    public function destroy(SubItem $subItem)
    {
      $subItem->status = 0;
      $subItem->save();

      return response()->json([
          'success' => 'success',
          'message' => 'ریکارد موفقانه حذف گردید.'
      ]);
    }
}
