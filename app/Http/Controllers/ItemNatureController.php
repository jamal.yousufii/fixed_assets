<?php

namespace App\Http\Controllers;

use App\ItemNature;
use Illuminate\Http\Request;
use App\Http\Requests\StoreAssetNature;
use App\Http\Resources\ItemNatureResource;

class ItemNatureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return ItemNatureResource::collection(ItemNature::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAssetNature $request)
    {
        /**
         * Generate a new Item Nature Code by counting records + 1
         * @var [type]
         */
          $custom_fields = array(
                'item_nature_code' => sprintf("%'.02d\n", ItemNature::count()+1),
                'status'     => 1,
                'created_by' => 1
          );

        ItemNature::create($request->all() + $custom_fields);
        return response()->json([
            'message' => 'نوعیت مصرفی جنس موفقانه ایجاد گردید.',
            'status'  => 'success'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ItemNature  $itemNature
     * @return \Illuminate\Http\Response
     */
    public function show(ItemNature $itemNature)
    {
        return new ItemNatureResource($itemNature);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ItemNature  $itemNature
     * @return \Illuminate\Http\Response
     */
    public function edit(ItemNature $itemNature)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ItemNature  $itemNature
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ItemNature $itemNature)
    {
        $itemNature->item_nature_dr = $request->item_nature_dr;
        $itemNature->item_nature_pa = $request->item_nature_pa;
        $itemNature->item_nature_en = $request->item_nature_en;
        $itemNature->save();
        return response()->json([
            'success' => 'success',
            'message' => 'نوعیت مصرفی جنس موفقانه تجدید گردید.'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ItemNature  $itemNature
     * @return \Illuminate\Http\Response
     */
    public function destroy(ItemNature $itemNature)
    {
        $itemNature->status = 0;
        $itemNature->save();

        return response()->json([
            'message' => 'ریکارد موفقانه حذف گردید.'
        ]);
    }
}
