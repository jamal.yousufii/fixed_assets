<?php

namespace App\Http\Controllers;

use App\Purchase;
use Illuminate\Http\Request;
use App\Http\Resources\PurchaseResource;
use App\Http\Resources\EndItemDetailsResource;
use App\StaticTable;
use App\EndItemDetails;

class PurchaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return PurchaseResource::collection(Purchase::with('currency')->with('measuringUnit')->with('endItemDetails')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $end_item_id = $request->input('end_item.id');
        $currency_id = $request->input('currency.id');
        $unit_id     = $request->input('unit.id');

        $custom_fields = array(
                            'end_item_details_id' => $end_item_id,
                            'currency_id' => $currency_id,
                            'unit' => $unit_id,
                            'created_by' => 1,
                            'order_by' => 1,
                            'authorized_by' => 1,
                         );
        $test = $request->except(['currency', 'end_item', 'unit'])+$custom_fields;


        $result = Purchase::create($test);
        // Store Remark
        Remark::create([
             'record_id' => $result->id,
             'remark'    => $request->remark,
             'section'   => 'purchase',
             'created_by'=> 1
        ]);

        return response()->json([
            'message' => 'جنس خریداری موفقانه ثبت گردید',
            'section' => 'purchase',
            'created_by' => 1,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new PurchaseResource(Purchase::with('currency')->with('measuringUnit')->with('remarks')->with(['endItemDetails','endItemDetails.endItem','endItemDetails.vendor'])->find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function edit(Purchase $purchase)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Purchase $purchase)
      {
        // /**
        //  * $item_nature_code, $main_item_code For generating the code for Main Item  e.g. 01, 02 => 01-02
        //  * @var [type]
        //  */
        // $item_nature_code = $request->input('item_nature.item_nature_code');
        // $main_item_code   = array_last(explode('-', $mainItem->main_item_code));
        // $request_inputs   = $request->all();
        //
        // $request_inputs['main_item_code'] = "$item_nature_code-$main_item_code";
        // $request_inputs['item_nature_id'] = $request->input('item_nature.id');
        // $mainItem->update($request_inputs);
        //
        // return response()->json([
        //     'success' => 'success',
        //     'message' => 'نوعیت مصرفی جنس موفقانه تجدید گردید.'
        // ]);
        // return $request->currency['id'];
        // return $request->end_item['id'];

        $end_item_id = $request->input('end_item.id');
        // $currency_id = $request->input('currency.id');
        $currency_id = $request->input('end_item.id');
        $unit_id = $request->input('unit.id');
        // return $unit_id;
        $custom_fields = array(
                            'end_item_details_id' => $end_item_id,
                            'currency_id' => $currency_id,
                            'unit' => $unit_id,
                            'created_by' => 1,
                            'order_by' => 1,
                            'authorized_by' => 1,
                         );
        // array_push($custom_fields, $request->all());
        $test = $request->except(['currency', 'end_item', 'unit'])+$custom_fields;
        // return $test;

        $purchase->update($test);

        return response()->json([
            'message' => 'جنس خریداری شده موفقانه تصحیح گردید.',
            'status'  => 'success'
        ]);
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function destroy(Purchase $purchase)
    {
        $purchase->delete();

        return response()->json([
            'success' => 'success',
            'message' => 'ریکارد موفقانه حذف گردید.'
        ]);
    }

    /**
     * Purchase options
     * @return array
     */
    public function purchaseOptions()
    {
      return [
        'end_item_details' => EndItemDetails::all(),
        'currency'         => StaticTable::where('type','currency')->get(),
        'unite'            => StaticTable::where('type','unite')->get(),
      ];
    }
}
