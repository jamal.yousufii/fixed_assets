<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\ItemInquireResource;
use App\ItemInquire;

class ItemInquireController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ItemInquireResource::collection(ItemInquire::with('inquireStatus')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request_input = json_decode($request->fields,true);
        $request_input['created_by'] = 1;
        $item_inquire = ItemInquire::create($request_input);
        // Upload the file Helper Function
        uploadFile($request,$item_inquire->id,'inquires');

        return response()->json([
            'message' => 'درخواستی برای جنس موفقانه ثبت گردید.',
            'status'  => 'success'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ItemInquire  $itemInquire
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new ItemInquireResource(ItemInquire::find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ItemInquire  $itemInquire
     * @return \Illuminate\Http\Response
     */
    public function edit(ItemInquire $itemInquire)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ItemInquire  $itemInquire
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {

      $item_inquire = ItemInquire::find($id);
      $item_inquire->update($request->all());

      return response()->json([
          'success' => 'success',
          'message' => 'جنس موفقانه تجدید گردید.'
      ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ItemInquire  $itemInquire
     * @return \Illuminate\Http\Response
     */
    public function destroy(ItemInquire $itemInquire)
    {
        //
    }
}
