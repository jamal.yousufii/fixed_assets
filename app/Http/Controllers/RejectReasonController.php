<?php

namespace App\Http\Controllers;

use App\RejectReason;
use Illuminate\Http\Request;

class RejectReasonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RejectReason  $rejectReason
     * @return \Illuminate\Http\Response
     */
    public function show(RejectReason $rejectReason)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RejectReason  $rejectReason
     * @return \Illuminate\Http\Response
     */
    public function edit(RejectReason $rejectReason)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RejectReason  $rejectReason
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RejectReason $rejectReason)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RejectReason  $rejectReason
     * @return \Illuminate\Http\Response
     */
    public function destroy(RejectReason $rejectReason)
    {
        //
    }
}
