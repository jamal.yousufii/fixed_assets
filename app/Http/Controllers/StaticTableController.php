<?php

namespace App\Http\Controllers;

use App\StaticTable;
use Illuminate\Http\Request;
use App\Http\Requests\StaticTableRequest;
use App\Http\Resources\StaticTableResource;

class StaticTableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($section,$id='')
    {
        if(!empty($id))
          return $this->show($section,$id);
        else
         return StaticTAbleResource::collection(StaticTable::where('type',$section)->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StaticTableRequest $request)
    {
        $code = ['code' => sprintf("%'.02d\n",StaticTable::where('type',$request->type)->count()+1)];
        StaticTable::create($request->all()+$code);

        return response()->json([
                'message' => 'کمپنی تولیدی موفقانه ثبت گردید.',
                'status'  => 'success'
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\StaticTable  $staticTable
     * @return \Illuminate\Http\Response
     */
    public function show($section,$id)
    {
       return new StaticTableResource(StaticTable::where('type',$section)->find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\StaticTable  $staticTable
     * @return \Illuminate\Http\Response
     */
    public function edit(StaticTable $staticTable)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\StaticTable  $staticTable
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $staticTable      = StaticTable::find($request->id);
        $staticTable->name_dr = $request->name_dr;
        $staticTable->name_en = $request->name_en;
        $staticTable->name_pa = $request->name_pa;
        $staticTable->save();

        return response()->json([
                'message' => 'معلومات موفقانه تجدید گردید.',
                'status'  => 'success'
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\StaticTable  $staticTable
     * @return \Illuminate\Http\Response
     */
    public function destroy($section,$id)
    {
        $staticTable = StaticTable::where('type',$section)->find($id);
        $staticTable->status = 0;
        $staticTable->save();

        return response()->json([
                'message' => 'معلومات موفقانه حذف گردید.',
                'status'  => 'success'
            ]);
    }
}
