<?php

namespace App\Http\Controllers;

use App\WriteOff;
use Illuminate\Http\Request;
use App\Inward;
use App\StaticTable;
use App\Allotment;

class WriteOffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $writeoff = new WriteOff;
      $writeoff->allotment_id = $request->input('inward_item.inward_id');
      $writeoff->assigned_by  = $request->input('assigned_by.id');
      $writeoff->assigned_to  = $request->input('assigned_to.id');
      $writeoff->fees8_number = $request->writeoff_number;
      $writeoff->write_off_date	  = $request->writeoff_date;
      $writeoff->sanction_by  = $request->input('sanction_by.id');
      $writeoff->number_of_item = $request->number_of_item;
      $writeoff->status = 1;
      $writeoff->created_by = 1;
      $writeoff->save();

      return response()->json([
          'message' => 'اجناس داغمه موفقانه ثبت گردید.',
          'status'  => 'success',
      ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\WriteOff  $writeOff
     * @return \Illuminate\Http\Response
     */
    public function show(WriteOff $writeOff)
    {
        return Allotment::with(['inward','inward.endItemDetails'])->find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\WriteOff  $writeOff
     * @return \Illuminate\Http\Response
     */
    public function edit(WriteOff $writeOff)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\WriteOff  $writeOff
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WriteOff $writeOff)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\WriteOff  $writeOff
     * @return \Illuminate\Http\Response
     */
    public function destroy(WriteOff $writeOff)
    {
        //
    }

    /**
     * Get Write-off options
     * @return array
     */
    public function getWriteOffOption()
    {
          $inward =  Inward::with('endItemDetails')->where('status','1')->get();
          $inward = $inward->toArray();
          $end_item_details = [];
          $i = 0;
          foreach($inward as $pr)
          {
             $end_item_details[$i] = $pr['end_item_details'];
             $end_item_details[$i]['inward_id'] = $pr['id'];
             $i++;
          }

         return [
            'directorate' => StaticTable::where('type','directorate')->get(),
            'department'  => StaticTable::where('type','department')->get(),
            'assigned_to' => StaticTable::where('type','store_keeper')->get(),
            'assigned_by' => StaticTable::where('type','assigned_to')->get(),
            'sanction_by' => StaticTable::where('type','sanction_by')->get(),
            'inward_item' => $end_item_details,
         ];
    }
}
