<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreAssetNature extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'item_nature_dr' => 'required|unique:item_natures',
            'item_nature_pa' => 'required|unique:item_natures',
            'item_nature_en' => 'required|unique:item_natures',
        ];
    }
    

     /**
     * Customize the Validation Message
     *
     * @return array
     */
    public function messages()
    {
        return [
            'item_nature_dr.required' => 'نوعیت مصرفی جنس به دری ضروری میباشد.',
            'item_nature_pa.required' => 'نوعیت مصرفی جنس به پشتو ضروری میباشد.',
            'item_nature_en.required' => 'نوعیت مصرفی جنس به انگلسی ضروری میباشد.',
            'item_nature_dr.unique'   => 'نوعیت مصرفی به دری از قبل وجود دارد.',
            'item_nature_pa.unique'   => 'نوعیت مصرفی به پشتو از قبل وجود دارد.',
            'item_nature_en.unique'   => 'نوعیت مصرفی به انگلسی از قبل وجود دارد.',
        ]; 
    }
    
}
