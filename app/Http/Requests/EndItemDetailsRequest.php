<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EndItemDetailsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'end_item.id' => 'required',
            'vendor.id'   => 'required',
            'end_item_details_dr'   => 'required',
            'end_item_details_pa'   => 'required',
            'end_item_details_en'   => 'required',
        ];
    }

    /**
    * Get the Validation Messages that apply to the request.
    *
    * @return array
    */
     public function messages()
     {
         return [
             'end_item.id.required'         => 'انتخاب جنس ضروری میباشد.',
             'vendor.id.required'           => 'انتخاب کمپنی ضروری میبشاد.',
             'end_item_details_dr.required' => 'جزیئات جنس به دری ضروری میباشد.',
             'end_item_details_pa.required' => 'جزیئات جنس به پشتو ضروری میباشد.',
             'end_item_details_en.required' => 'جزیئات جنس به انگلسی ضروری میباشد.',
         ];
     }


}
