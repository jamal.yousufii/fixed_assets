<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sub_item' => 'required',
            'end_item_name_dr' => 'required',
            'end_item_name_pa' => 'required',
            'end_item_name_en' => 'required',
        ];
    }

    /**
    * Get the Validation Messages that apply to the request.
    *
    * @return array
    */
     public function messages()
     {
         return [
             'sub_item.required'         => 'نوعیت مصرفی جنس ضروری میباشد.',
             'end_item_name_dr.required' => 'نام جنس به دری ضروری میباشد.',
             'end_item_name_pa.required' => 'نام جنس به پشتو ضروری میباشد.',
             'end_item_name_en.required' => 'نام جنس به انگلسی ضروری میباشد.',
         ];
     }

}
