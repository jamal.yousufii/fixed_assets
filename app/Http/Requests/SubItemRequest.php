<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SubItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'main_item' => 'required',
            'sub_item_name_dr' => 'required|unique:sub_items',
            'sub_item_name_pa' => 'required|unique:sub_items',
            'sub_item_name_en' => 'required|unique:sub_items',
        ];
    }

    /**
    * Get the Validation Messages that apply to the request.
    *
    * @return array
    */
     public function messages()
     {
         return [
             'main_item.required'        => 'نوعیت مصرفی جنس ضروری میباشد.',
             'sub_item_name_dr.required' => 'نام فرعی جنس به دری ضروری میباشد.',
             'sub_item_name_pa.required' => 'نام فرعی جنس به پشتو ضروری میباشد.',
             'sub_item_name_en.required' => 'نام جنس فرعی به انگلسی ضروری میباشد.',
             'sub_item_name_dr.unique'   => 'نام جنس فرعی به دری از قبل وجود دارد.',
             'sub_item_name_pa.unique'   => 'نام جنس فرعی به پشتو از قبل وجود دارد',
             'sub_item_name_en.unique'   => 'نام جنس فرعی به انگلسی از قبل وجود دارد',
         ];
     }

}
