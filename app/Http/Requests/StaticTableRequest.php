<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StaticTableRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_en' => 'required',
            'name_dr' => 'required',
            'name_pa' => 'required',
        ];
    }

    /**
    * Get the Validation Messages that apply to the request.
    *
    * @return array
    */
     public function messages()
     {
         return [
           'name_dr.required' => 'نام به دری ضروری میباشد.',
           'name_en.required' => 'نام به انگلسی ضروری میباشد.',
           'name_pa.required' => 'نام به پشتو ضروری میباشد.',
         ];
     }
}
