<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MainItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'main_item_name_dr' => 'required|unique:main_items',
            'main_item_name_pa' => 'required|unique:main_items',
            'main_item_name_en' => 'required|unique:main_items',
        ];
    }

     /**
     * Get the Validation Messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'main_item_name_dr.required' => 'نام جنس عمومی به دری ضروری میباشد.',
            'main_item_name_pa.required' => 'نام جنس عمومی به پشتو ضروری میباشد.',
            'main_item_name_en.required' => 'نام جنس عمومی به انگلسی ضروری میباشد.',
            'main_item_name_dr.unique' => 'نام جنس عمومی به دری از قبل وجود دارد.',
            'main_item_name_pa.unique' => 'نام جنس عمومی به پشتو از قبل وجود دارد',
            'main_item_name_en.unique' => 'نام جنس عمومی به انگلسی از قبل وجود دارد.',
        ];
    }


}
