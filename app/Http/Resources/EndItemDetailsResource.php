<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class EndItemDetailsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
           'id'               => $this->id,
           'item_details_code'=> $this->item_details_code,
           'end_item_details_dr' => $this->end_item_details_dr,
           'end_item_details_pa' => $this->end_item_details_pa,
           'end_item_details_en' => $this->end_item_details_en,
           'status'           => $this->status,
           'created_by'       => $this->created_by,
           'end_item'         => new EndItemResource($this->whenLoaded('endItem')),
           'vendor'           => new StaticTableResource($this->whenLoaded('vendor'))
        ];
    }
}
