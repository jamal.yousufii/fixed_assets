<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class StaticTableResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
     public function toArray($request)
     {
         return [
              'id'      => $this->id,
              'code'    => $this->code,
              'name_en' => $this->name_en,
              'name_pa' => $this->name_pa,
              'name_dr' => $this->name_dr,
              'type'    => $this->type,
         ];
     }
}
