<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MainItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                => $this->id,
            'item_nature_id'    => $this->item_nature_id,
            'main_item_code'    => $this->main_item_code,
            'main_item_name_en' => $this->main_item_name_en,
            'main_item_name_dr' => $this->main_item_name_dr,
            'main_item_name_pa' => $this->main_item_name_pa,
            'status'            => $this->status,
            'item_nature'        => new ItemNatureResource($this->whenLoaded('itemNature'))
        ];
    }
}
