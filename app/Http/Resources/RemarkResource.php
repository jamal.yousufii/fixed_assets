<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class RemarkResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
      return [
          'record_id'  => $this->record_id,
          'remark'     => $this->remark,
          'section'    => $this->section,
          'created_by' => $this->created_by,
      ];
    }
}
