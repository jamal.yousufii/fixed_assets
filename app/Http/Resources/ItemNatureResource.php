<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ItemNatureResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'item_nature_code' => $this->item_nature_code,
            'item_nature_dr' => $this->item_nature_dr,
            'item_nature_pa' => $this->item_nature_pa,
            'item_nature_en' => $this->item_nature_en,
            'status' => $this->status,
        ];
    }
}
