<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PurchaseResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
          'id' => $this->id,
          'order_number' => $this->order_number,
          'order_date'   => $this->order_date,
          'unit_price'   => $this->unit_price,
          // 'currency' => $this->currency_id,
          'currency'        => new StaticTableResource($this->whenLoaded('currency')),
          'number_of_item'  => $this->number_of_item,
          'end_item'        => new EndItemDetailsResource($this->whenLoaded('endItemDetails')),
          'unit'            => new StaticTableResource($this->whenLoaded('measuringUnit')),
          'remarks'         => new RemarkResource($this->whenLoaded('remarks')),
        ];
    }
}
