<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SubItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                => $this->id,
            'main_item_id'      => $this->main_item_id,
            'sub_item_code'     => $this->sub_item_code,
            'sub_item_name_en'  => $this->sub_item_name_en,
            'sub_item_name_dr'  => $this->sub_item_name_dr,
            'sub_item_name_pa'  => $this->sub_item_name_pa,
            'status'            => $this->status,
            'main_item'         => new MainItemResource($this->whenLoaded('mainItem'))
        ]; 
    }
}
