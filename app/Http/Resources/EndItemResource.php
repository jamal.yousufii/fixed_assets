<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class EndItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
      return [
          'id' => $this->id,
          'sub_item_id' => $this->sub_item_id,
          'end_item_code' => $this->end_item_code,
          'end_item_name_en' => $this->end_item_name_en,
          'end_item_name_dr' => $this->end_item_name_dr,
          'end_item_name_pa' => $this->end_item_name_pa,
          'status' => $this->status,
          'sub_item' => new SubItemResource($this->whenLoaded('subItem')),
          'vendor'   => new StaticTableResource($this->whenLoaded('vendor'))
        ];

    }
}
