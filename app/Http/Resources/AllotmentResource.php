<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AllotmentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'inward_id' => $this->inward_id,
            'assigned_by' => $this->assigned_by,
            'assigned_to' => $this->assigned_to,
            'fees5_number' => $this->fees5_number,
            'allot_date' => $this->allot_date,
            'sanction_by' => $this->sanction_by,
            'type' => $this->type,
        ];
    }
}
