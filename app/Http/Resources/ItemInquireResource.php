<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ItemInquireResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                  => $this->id,
            'item_inquire_number' => $this->item_inquire_number,
            'item_inquire_desc'   => $this->item_inquire_desc,
            'item_inquire_date'   => $this->item_inquire_date,
            'created_by'          => $this->created_by,
            'status'              => $this->status,
            'inquire_status'      => new StaticTableResource($this->whenLoaded('inquireStatus'))
        ];
    }
}
