<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubItem extends Model
{
     use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['main_item_id','sub_item_code','sub_item_name_en','sub_item_name_dr','sub_item_name_pa','status','created_by'];

    /*
    * Get the Main Item that owns the Sub Item
    *
    */
    public function mainItem()
    {
        return $this->belongsTo('App\MainItem');
    }

    /*
    * Get the End Item for the Sub Item
    *
    */
    public function endItem()
    {
        return $this->hasMany('App\EndItem');
    }

}
