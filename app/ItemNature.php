<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemNature extends Model
{
     use SoftDeletes;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
   protected $fillable = ['item_nature_code','item_nature_en','item_nature_dr','item_nature_pa','status','created_by'];


    /*
    * Get the Main Item for the Item Nature
    *
    */
    public function mainItem(){
        return $this->hasMany('App\MainItem');
    }
}
