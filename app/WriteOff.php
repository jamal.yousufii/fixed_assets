<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WriteOff extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['allotment_id','assigned_by','assigned_to','number_of_item','sanction_by','fees8_number','write_off_date', 'status', 'created_by'];

    use SoftDeletes;

}
