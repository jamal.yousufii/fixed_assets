<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MainItem extends Model
{
     use SoftDeletes;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     protected $fillable = ['item_nature_id','main_item_code','main_item_name_en','main_item_name_dr','main_item_name_pa','status','created_by'];

    /*
     * Get the Item Nature that owns the Main Item
     *
     */
    public function itemNature(){
        return $this->belongsTo('App\ItemNature');
    }

    /**
     * Get the Sub Item for the Main Item
     *
     */
    public function subItem(){
        return $this->hasMany('App\SubItem');
    }

}
