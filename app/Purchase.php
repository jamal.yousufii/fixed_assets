<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Purchase extends Model
{
     use SoftDeletes;

     /**
      * The attributes that are mass assignable.
      *
      * @var array
      */
     protected $fillable = ['end_item_details_id','currency_id','order_number','order_date','order_by','number_of_item','unit', 'unit_price', 'authorized_by', 'created_by'];

    // Get End Item Detilas which Own purchase
    public function endItemDetails()
    {
        return $this->belongsTo('App\EndItemDetails');
    }


    /*
    * Get Inward for the purchase
    *
    */
    public function inward()
    {
        return $this->hasMany('App\Inward');
    }



    /*
    * Get the vendor that owns the End Items
    *
    */
    public function measuringUnit()
    {
        return $this->belongsTo('App\StaticTable','unit','id');
    }

    /*
    * Get the vendor that owns the End Items
    *
    */
    public function currency()
    {
        return $this->belongsTo('App\StaticTable','currency_id','id');
    }

    /**
     * Get Remarks
     * @return object
     */
    public function remarks()
    {
       return $this->belongsTo('App\Remark','id','record_id')->where('section','purchase');
    }
}
