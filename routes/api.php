<?php

use Illuminate\Http\Request;
use Illuminate\Notifications\RoutesNotifications;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('item_nature', 'ItemNatureController')->except(['create','edit']);
// Main Item Resource Route
Route::resource('main_item', 'MainItemController')->except(['create','edit']);
// Sub Item Resource Router
Route::resource('sub_item','SubItemController')->except(['create','edit']);
// End Item resource route
Route::resource('end_item','EndItemController')->except(['create','edit']);
// End Item Details resource route
Route::resource('end_item_details','EndItemDetailsController')->except(['create','edit']);

// End Item Details resource route
Route::resource('purchase','PurchaseController')->except(['create','edit']);
// Item Inquire Resource RoutesNotifications
Route::resource('inquire','ItemInquireController')->except(['create','edit']);
// Inward Resource Route
Route::resource('inward','InwardController')->except(['create','edit']);
// Allotment Resource RoutesNotifications
Route::resource('allotment','AllotmentController')->except(['create','edit']);
// Write-off Resource Route
Route::resource('writeoff','WriteOffController')->except(['create','edit']);

// list Inquire of Items
// Route::get('inquire_list','ItemInquire@index');
// Get Option for End Details Item
Route::get('end_details_item_option','EndItemDetailsController@endDetailsItemOption');
// Static Table Route
Route::get('static/{slug?}/{id?}','StaticTableController@index');
Route::post('static/{slug}','StaticTableController@store');
Route::put('static/{slug}/{id}','StaticTableController@update');
Route::delete('static/{slug}/{id}','StaticTableController@destroy');
// Get Public Data
Route::post('get_public_data','PublicController@getPublicData');
// Get Purchase options
Route::get('get_purchase_options','PurchaseController@purchaseOptions');
// Get Inward Options
Route::get('get_inward_options','InwardController@getInwardOptions');
// Get allotment Options
Route::get('get_allotment_options','AllotmentController@getAllotmentOptions');
// Get Details of End Item
Route::post('inward_details','InwardController@inwardDetails');
// Get Write-off Options
Route::get('get_writeoff_options','WriteOffController@getWriteOffOption');
