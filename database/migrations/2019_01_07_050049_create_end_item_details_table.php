<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEndItemDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('end_item_details', function (Blueprint $table) {
          $table->increments('id');
          $table->unsignedMediumInteger('vendor_id');
          $table->unsignedMediumInteger('end_item_id');
          $table->string('item_details_code',256);
          $table->string('end_item_details_dr');
          $table->string('end_item_details_pa');
          $table->string('end_item_details_en');
          $table->string('status',32);
          $table->integer('created_by');
          $table->timestamps();
          $table->softDeletes();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('end_item_details');
    }
}
