<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropAssignedIdFromWriteOffs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('write_offs', function (Blueprint $table) {
            $table->dropColumn('assigned_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('write_offs', function (Blueprint $table) {
            $table->unsignedInteger('assigned_id'); 
        });
    }
}
