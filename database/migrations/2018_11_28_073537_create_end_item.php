<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEndItem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('end_items', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedMediumInteger('sub_item_id');
            $table->string('end_item_code',256);
            $table->string('end_item_name_en', 128);
            $table->string('end_item_name_dr', 128);
            $table->string('end_item_name_pa', 128);
            $table->string('status',32);
            $table->integer('created_by');
            $table->timestamps();
            $table->softDeletes();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('end_items');
    }
}
