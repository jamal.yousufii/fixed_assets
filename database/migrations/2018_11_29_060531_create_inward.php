<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInward extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inwards', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('purchase_id');
            $table->unsignedInteger('item_details_id');
            $table->string('m7_number', 128);
            $table->string('warranty', 32);
            $table->date('inward_date');
            $table->unsignedMediumInteger('dept_id');
            $table->unsignedInteger('store_keeper_id');
            $table->unsignedInteger('authorized_by');
            $table->unsignedMediumInteger('number_of_item');
            $table->mediumInteger('remain');
            $table->unsignedInteger('created_by');
            $table->string('status', 32);
            $table->timestamps();
            $table->softDeletes();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inwards');
    }
}
