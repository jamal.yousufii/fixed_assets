<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchases', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedMediumInteger('end_item_details_id');
            $table->unsignedSmallInteger('currency_id');
            $table->string('order_number');
            $table->date('order_date');
            $table->string('order_by');
            $table->unsignedSmallInteger('number_of_item');
            $table->unsignedSmallInteger('unit');
            $table->unsignedInteger('unit_price');
            $table->unsignedInteger('authorized_by');
            $table->unsignedInteger('created_by');
            $table->timestamps();
            $table->softDeletes();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchases');
    }
}
