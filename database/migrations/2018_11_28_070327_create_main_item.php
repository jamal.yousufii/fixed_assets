<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMainItem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('main_items', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedMediumInteger('item_nature_id');
            $table->string('main_item_code',128);
            $table->string('main_item_name_en', 128);
            $table->string('main_item_name_dr', 128);
            $table->string('main_item_name_pa', 128);
            $table->string('status',64);
            $table->unsignedInteger('created_by');
            $table->timestamps();
            $table->softDeletes();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('main_items');
    }
}
