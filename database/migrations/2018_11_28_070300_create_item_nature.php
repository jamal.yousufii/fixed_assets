<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemNature extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_natures', function (Blueprint $table) {
            $table->increments('id');
            $table->string('item_nature_en');
            $table->string('item_nature_dr');
            $table->string('item_nature_pa');
            $table->string('status',64);
            $table->unsignedInteger('created_by');
            $table->timestamps();
            $table->softDeletes();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_natures');
    }
}
