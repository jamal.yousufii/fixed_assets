<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToAllotment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('allotments', function (Blueprint $table) {
            $table->string('fees5_number',124)->nullable()->after('assigned_to');
            $table->date('allot_date')->after('fees5_number');
            $table->mediumInteger('sanction_by')->unsigned()->after('allot_date');
            $table->enum('type',['1','2','3'])->after('number_of_item')->comment('1 is allotment 2 is allotment to individual 3 is allotment to storekeeper');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('allotments', function (Blueprint $table) {
            $table->dropColumn(['fees5_number', 'allot_date', 'sanction_by','type']);
        });
    }
}
