<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubItem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_items', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedMediumInteger('main_item_id');
            $table->string('sub_item_code', 128);
            $table->string('sub_item_name_en', 128);
            $table->string('sub_item_name_dr', 128);
            $table->string('sub_item_name_pa', 128);
            $table->unsignedInteger('created_by');
            $table->string('status',64);
            $table->timestamps();
            $table->softDeletes();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_items');
    }
}
