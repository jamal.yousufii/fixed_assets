<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAllotment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('allotments', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('inward_id');
            $table->unsignedInteger('assigned_by');
            $table->unsignedInteger('assigned_to');
            $table->unsignedSmallInteger('number_of_item');
            $table->string('status',32);
            $table->unsignedInteger('created_by');
            $table->timestamps();
            $table->softDeletes();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('allotments');
    }
}
