<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemInquiresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_inquires', function (Blueprint $table) {
            $table->increments('id');
            $table->string('item_inquire_number');
            $table->longText('item_inquire_desc');
            $table->date('item_inquire_date');
            $table->unsignedInteger('created_by');
            $table->enum('status',[1,2,3,4])->default(1)->comment('1 is Print, 2 is in process, 3 is Proceed, 4 is Rejected');
            $table->timestamps();
            $table->softDeletes();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_inquires');
    }
}
