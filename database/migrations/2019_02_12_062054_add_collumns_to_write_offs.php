<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCollumnsToWriteOffs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('write_offs', function (Blueprint $table) {
            $table->unsignedInteger('assigned_by')->after('allotment_id');
            $table->unsignedInteger('assigned_to')->after('assigned_by');
            $table->unsignedMediumInteger('number_of_item')->after('assigned_to');
            $table->unsignedInteger('fees8_number')->after('number_of_item');
            $table->unsignedInteger('sanction_by')->after('number_of_item');
            $table->unsignedInteger('created_by')->after('status');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('write_offs', function (Blueprint $table) {
          $table->dropColumn('assigned_by');
          $table->dropColumn('assigned_to');
          $table->dropColumn('number_of_item');
          $table->dropColumn('sanction_by');
          $table->dropColumn('fees8_number');
          $table->dropColumn('created_by');
        });
    }
}
