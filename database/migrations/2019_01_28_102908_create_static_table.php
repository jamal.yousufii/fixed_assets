<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaticTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('static_tables', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 128);
            $table->string('name_en', 128);
            $table->string('name_pa', 128);
            $table->string('name_dr', 128);
            $table->string('type', 128);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('static_tables');
    }
}
